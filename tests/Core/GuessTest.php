<?php

namespace App\Tests\Core;

use App\Core\Card;
use PHPUnit\Framework\TestCase;
use App\Core\Guess;

class GuessTest extends TestCase
{
    //Should return "La carte a trouver à une valeur inférieure!";
    public function testGuessInf()
    {
        $selectedCard = new Card('4', 'Coeur');
        $submittedCard = new Card('3', 'Coeur');
        $result = Guess::guessCard($selectedCard, $submittedCard);
        $this->assertEquals("La carte a trouver à une valeur inférieure!", $result);
    }

    //Should return "La carte a trouver à une valeur supérieure!"
    public function testGuessSup()
    {
        $selectedCard = new Card('3', 'Coeur');
        $submittedCard = new Card('4', 'Coeur');
        $result = Guess::guessCard($selectedCard, $submittedCard);
        $this->assertEquals("La carte a trouver à une valeur supérieure!", $result);
    }

    //Should return "La carte a été trouvée!"
    public function testGuessFound()
    {
        $selectedCard = new Card('3', 'Coeur');
        $submittedCard = new Card('3', 'Coeur');
        $result = Guess::guessCard($selectedCard, $submittedCard);
        $this->assertEquals("La carte a été trouvée!", $result);
    }
}