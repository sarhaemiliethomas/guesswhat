<?php

namespace App\Core;

use App\Core\Card;
use Webmozart\Assert\Assert;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{
    /**
     * @var $cards array an array of Cards
     */
    private $cards;

    /**
     * @var $selectedCard Card This is the card to be guessed by the player
     */
    private $selectedCard;

    public static function guessCard(Card $selectedCard, Card $submittedCard) : string
    {
        $resultCmp = Card::cmp($selectedCard, $submittedCard);
        $result = null;

        switch ($resultCmp) {
            case 0:
                $result = "La carte a été trouvée!";
                break;
            case -1:
                $result = "La carte a trouver à une valeur supérieure!";
                break;
            case +1:
                $result = "La carte a trouver à une valeur inférieure!";
                break;
        }

        return $result;
    }
}