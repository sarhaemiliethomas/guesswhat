<?php

namespace App\Core;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{
  /**
   * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
   */
    private $name;

    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;

    /**
     * Card constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $name
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /** définir une relation d'ordre entre instance de Card.
     *  Remarque : cette méthode n'est pas de portée d'instance
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     *
     * @param $selectedCard Card
     * @param $submittedCard Card
     * @return int
     * <ul>
     *  <li> zéro si $selectedCard et $submittedCard sont considérés comme égaux </li>
     *  <li> -1 si $selectedCard est considéré inférieur à $submittedCard</li>
     * <li> +1 si $selectedCard est considéré supérieur à $submittedCard</li>
     * </ul>
     *
     */
    public static function cmp(Card $selectedCard, Card $submittedCard) : int
    {
        $cardValue = [
            "coeur"=>3,
            "carreau"=>2,
            "pique"=>1,
            "trèfle"=>0
        ];

        $cardName = [
            "2" => 1,
            "3" => 2,
            "4" => 3,
            "5" => 4,
            "6" => 5,
            "7" => 6,
            "8" => 7,
            "9" => 8,
            "10" => 9,
            "valet" => 10,
            "dame" => 11,
            "roi" => 12,
            "as" => 13
        ];

        $selectedCardColor = $cardValue[strtolower($selectedCard->color)];
        $submittedCardColor = $cardValue[strtolower($submittedCard->color)];

        $selectedCardName = $cardName[strtolower($selectedCard->name)];
        $submittedCardName = $cardName[strtolower($submittedCard->name)];

//      Essai pour améliorer la robustesse du code contre les valeurs incorrectes
//      if (($selectedCardColor != null) || ($selectedCardColor >= 4) || ($selectedCardColor < 0)) {

        if ($selectedCardName == $submittedCardName) {
            if ($selectedCardColor == $submittedCardColor) {
                return 0;
            }
            return ($selectedCardColor > $submittedCardColor) ? +1 : -1;
        }
        else {
            return ($selectedCardName > $submittedCardName) ? +1 : -1;
        }
//      }
//        return 2;
    }
}